package main

import (
	"github.com/gin-gonic/gin"
	"fmt"
)

func main() {
	urlMap := make(map[string]bool)
	urlMap["localhost"] = true
	urlMap["cherry.gracetheme.com"] = true
	r := gin.Default()
	r.GET("/cherry-auth", func(c *gin.Context) {
		site := c.Request.URL.Query().Get("verify_url")
		fmt.Println(site)
		if urlMap[site] {
			c.String(200, "verify_success")
		} else {
			c.String(200, "verify_fail")
		}
	})
	r.Run(":8888")
}
